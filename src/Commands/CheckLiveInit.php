<?php

namespace Contus\LiveStream\Commands;

use Illuminate\Console\Command;
use Contus\Livestream\Repositories\LiveStreamRepository;

class CheckLiveInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'live:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks the scheduled live stream and initiates the streams.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkAndInitLive();
        $this->checkAndUpdateStatus();
    }

    private function checkAndInitLive(){
        try {
            (new LiveStreamRepository)->initLiveVideos();
        }
        catch ( Exception $exception ) {
            app ('log' )->error ( ' ###File : '.$exception->getFile () .' ##Line : '.$exception->getLine () .' #Error : '. $exception->getMessage () );
        }
    }

    private function checkAndUpdateStatus(){
        try {
            (new LiveStreamRepository)->statusLiveStreamAll();
        }
        catch ( Exception $exception ) {
            app ('log' )->error ( ' ###File : '.$exception->getFile () .' ##Line : '.$exception->getLine () .' #Error : '. $exception->getMessage () );
        }
    }
}
