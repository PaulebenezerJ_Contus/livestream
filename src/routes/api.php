<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group ( [ 'prefix' => 'api/admin/livestream','namespace' => 'Contus\Livestream\Api\Controllers\Admin' ], function () {
    Route::group ( [ 'middleware' => 'api.admin' ], function () {
        /*Livestream Routes Start*/
        Route::get('info', 'LivestreamController@getInfo');
        Route::get('startStream/{id}','LivestreamController@startStream');
        Route::get('stopStream/{id}','LivestreamController@stopStream');
        Route::get('{id}', 'LivestreamController@getLivestream');
        Route::post('records', 'LivestreamController@postRecords');
        Route::post('upload', 'LivestreamController@postUploads');
        Route::post('add', 'LivestreamController@postAdd');
        Route::post('action', 'LivestreamController@postAction');
        Route::post('update-status/{id}', 'LivestreamController@postUpdateStatus');
    } );
} );
