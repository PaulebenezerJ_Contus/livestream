<?php

/**
 * Admin User Group
 *
 * This model will going to hold the table related to admin users groups and its relations
 * Contians the common validations used by this model
 *
 * @category  Contus
 * @package   Contus_laravel 5.3
 * @author    Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2016 Contus. All rights reserved.
 * @license   GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @version   Release: 1.0
 */
namespace Contus\Livestream\Models;

use Contus\Base\Model;

class Livestream extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'livestream';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'is_active',
        'thumburl',
        'max_height',
    ];

    public function getThumbnailAttribute()
    {
        if ($this->thumburl) {
            return env('S3_IMAGE_BASE_URL') . '/' . $this->thumburl;
        }
        return '';
    }

    /**
     * funtion to automate operations while Saving
     */
    public function bootSaving() {
        $this->setDynamicSlug ( 'title' );
    }

}
