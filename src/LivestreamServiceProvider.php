<?php

/**
 * Video Service Provider which defines all information about the livestream package.
 *
 * @name VideoServiceProvider
 * @vendor Contus
 * @package Video
 * @version 1.0
 * @author Contus<developers@contus.in>
 * @copyright Copyright (C) 2016 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Livestream;

use Illuminate\Support\ServiceProvider;
use Contus\Base\Helpers\StringLiterals;
use Illuminate\Console\Scheduling\Schedule;

class LivestreamServiceProvider extends ServiceProvider {
    
    protected $commands = [
        Commands\CheckLiveInit::class,
    ];
    /**
     * Bootstrap the application services.
     *
     * @vendor Contus
     *
     * @package Video
     * @return void
     */
    public function boot() {
        $Livestream = 'livestream';
        $this->loadScheduler();
        $this->loadTranslationsFrom ( __DIR__ . DIRECTORY_SEPARATOR . StringLiterals::RESOURCES . DIRECTORY_SEPARATOR . 'lang', $Livestream );
        $this->loadViewsFrom ( __DIR__ . DIRECTORY_SEPARATOR . StringLiterals::RESOURCES . DIRECTORY_SEPARATOR . 'views', $Livestream );
        $this->publishes ( [ __DIR__ . DIRECTORY_SEPARATOR . StringLiterals::RESOURCES . DIRECTORY_SEPARATOR . 'assets' => public_path ( 'contus/'.$Livestream ) ], $Livestream.'_assets' );
        $this->publishes ( [ __DIR__ . DIRECTORY_SEPARATOR . 'config' => config_path ( 'contus/'.$Livestream ) ], $Livestream.'_config' );
        $this->shareDataToView ();
    }
    
    private function loadScheduler(){
        $this->app->booted(function (){
            $schedule = $this->app->make(Schedule::class);
            $schedule->command(Commands\CheckLiveInit::class)->everyMinute();
        });
    }

    /**
     * Register the application services.
     *
     * @vendor Contus
     *
     * @package User
     * @return void
     */
    public function register() {
        include __DIR__ . '/routes/web.php';
        include __DIR__ . '/routes/api.php';
        $this->commands($this->commands);
    }
    
    /**
     * Method used to share the data to blade file.
     *
     * Can access getUrl, auth, siteSettings in view files.
     *
     * @return void
     */
    public function shareDataToView() {
        view ()->share ( 'getLivestreamAssetsUrl', function ($url = '/') {
            return url ( config ( 'contus.livestream.livestream.vendor' ) . '/' . config ( 'contus.livestream.livestream.package' ) . '/' . $url );
        } );
    }
}
