<?php

/**
 * Template Controller for Ads package
 *
 * To manage Templates.
 *
 * @name       Template Controller
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2018 Contus. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Livestream\Http\Controllers\Admin;

use Contus\Base\Controller as BaseController;
use Contus\Base\Handlers\TemplateHandler;

class LivestreamTemplateController extends BaseController {
    use TemplateHandler;
    /**
     * class construct function
     * 
     */
    public function __construct() {
        parent::__construct();
        $this->setPackage(config ( 'contus.livestream.livestream.package' ))->setFolder(config ( 'contus.livestream.livestream.package' ));
    }
}
