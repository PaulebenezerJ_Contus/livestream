<?php

/**
 * SmilFile for wowza
 *
 * To manage the SmilFile
 *
 * @name SmilFile
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */

namespace Contus\Livestream\Lib\Wowza;

use Com\Wowza\SmilFile as WowzaSmilFile;

class SmilFile extends WowzaSmilFile
{
    use Traits\WowzaDefault;

    /**
     * function construct
     * 
     */
    public function __construct()
    {
        $this->initWowzaSettings();
        parent::__construct($this->wowza, env('WOWZA_APPLICATION'));
    }

    /**
     * function reset RestURI Publisher
     * 
     */
    private function resetRestURISmilFile(){
        $this->resetRestURI();
        $this->restURI = $this->restURI. "/vhosts/" . $this->getVHostInstance() . "/applications/" . env('WOWZA_APPLICATION') . "/smilfiles";
    }

    public function addSmilFile($fileName, $streams)
    {
        $this->resetRestURISmilFile();
        $this->restURI = $this->restURI . "/" . $fileName;
        $this->smilStreams = $streams;
        $this->addAdditionalParameter('title', $fileName)->addAdditionalParameter('name', $fileName);
        $response = $this->sendRequest($this->preparePropertiesForRequest(self::class), []);
        return $response;
    }

    public function updateSmilFile($fileName, $streams)
    {
        $this->resetRestURISmilFile();
        $this->restURI = $this->restURI . "/" . $fileName;
        $this->smilStreams = $streams;
        $this->addAdditionalParameter('title', $fileName)->addAdditionalParameter('name', $fileName);
        $response = $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_PUT);
        return $response;
    }

    public function updateSmilFileAction($fileName,$action)
    {
        $this->resetRestURISmilFile();
        $this->restURI = $this->restURI . "/" . $fileName.'/actions/'.$action;
        $this->addSkipParameter('title', true)->addSkipParameter('name', true)->addSkipParameter('smilStreams', true);
        $response = $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_PUT);
        return $response;
    }

    public function deleteSmilFileAction($fileName)
    {
        $this->resetRestURISmilFile();
        $this->restURI = $this->restURI . '/' . $fileName;
        $this->addSkipParameter('title', true)->addSkipParameter('name', true)->addSkipParameter('smilStreams', true);
        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_DELETE);
    }
    /* protected $smilStreams = [];

    public function __construct(Settings $settings, $appName)
    {
        parent::__construct($settings);
        $this->restURI = $this->getHost() . "/servers/" . $this->getServerInstance() . "/vhosts/" . $this->getVHostInstance() . "/applications/" . $appName . "/smilfiles";
    }

    public function create($fileName, $streams)
    {
        $this->restURI = $this->restURI . "/" . $fileName;
        $this->smilStreams = $streams;

        $response = $this->sendRequest($this->preparePropertiesForRequest(self::class), []);

        return $response;
    }

    public function get($fileName)
    {
        $this->addSkipParameter('smilStreams', true);
        $this->restURI = $this->restURI . "/" . $fileName;

        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_GET);
    }

    public function getAll()
    {
        $this->addSkipParameter('smilStreams', true);

        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_GET);
    }

    public function remove($fileName)
    {
        $this->addSkipParameter('smilStreams', true);
        $this->restURI = $this->restURI . '/' . $fileName;

        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_DELETE);
    } */
}
