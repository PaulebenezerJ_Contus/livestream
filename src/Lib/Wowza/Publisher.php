<?php

/**
 * Publisher for wowza
 *
 * To manage the Publishers
 *
 * @name Publisher
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Livestream\Lib\Wowza;

use Com\Wowza\Publisher as WowzaPublisher;

class Publisher extends WowzaPublisher {
    use Traits\WowzaDefault;

    /**
     * function construct
     * 
     */
    public function __construct()
    {
        $this->initWowzaSettings('v3');
        parent::__construct($this->wowza);
    }
    /**
     * function reset RestURI Publisher
     * 
     */
    private function resetRestURIPublisher(){
        $this->resetRestURI();
        $this->restURI = $this->restURI  . "/publishers";
    }

    /**
     * function to set Publisher
     * 
     * @param string $publisherName 
     */
    private function setPublisher($publisherName){
        $this->name = $publisherName;
        $this->resetRestURIPublisher();
        $this->restURI = $this->restURI . "/" . $this->name;
    }

    /**
     * function to get Publisher
     * 
     * @param string $publisherName
     * 
     * @return mixed 
     */
    public function getPublisher($publisherName)
    {
        $this->setPublisher($publisherName);
        $this->addSkipParameter('name', true)->addSkipParameter('password', true);
        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_GET);
    }

    /**
     * function to add Publisher
     * 
     * @param string $publisherName 
     * @param string publisherPassword 
     * 
     * @return mixed 
     */
    public function addPublisher($publisherName,$publisherPassword)
    {
        $this->setPublisher($publisherName);
        $this->addAdditionalParameter('name', $publisherName);
        $this->addAdditionalParameter('password', $publisherPassword);
        return $this->sendRequest($this->preparePropertiesForRequest(self::class), []);
    }

    /**
     * Function to update Publisher
     * 
     * @param string $publisherName 
     * @param string $publisherPassword 
     * 
     * @return mixed 
     */
    public function updatePublisher($publisherName,$publisherPassword)
    {
        $this->setPublisher($publisherName);
        $this->addAdditionalParameter('name', $publisherName);
        $this->addAdditionalParameter('password', $publisherPassword);
        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_PUT);
    }

    /**
     * function to delete Publisher
     * 
     * @param string $publisherName 
     * 
     * @return mixed 
     */
    public function deletePublisher($publisherName)
    {
        $this->setPublisher($publisherName);
        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_DELETE);
    }

    /**
     * function to get All Publishers
     * 
     * @return mixed 
     */
    public function getAllPublishers()
    {
        $this->resetRestURIPublisher();
        return $this->getAll();
    }
}