<?php

/**
 * Transcoder for wowza
 *
 * To manage the Transcoder
 *
 * @name Transcoder
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Livestream\Lib\Wowza;

use Com\Wowza\Wowza; 

class Transcoder extends Wowza
{
    use Traits\WowzaDefault;

    public function __construct()
    {
        $this->initWowzaSettings();
        parent::__construct($this->wowza);
    }

    /**
     * function reset RestURI Publisher
     * 
     */
    private function resetRestURITranscoderTemplates(){
        $this->resetRestURI();
        $this->restURI = $this->restURI. '/vhosts/'.$this->getVHostInstance() . "/applications/" . env('WOWZA_APPLICATION') . "/transcoder/templates";
    }

    public function getTemplate($templateName){
        $this->resetRestURITranscoderTemplates();
        $this->restURI = $this->restURI . '/'. $templateName;
        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_GET);
    }
}
