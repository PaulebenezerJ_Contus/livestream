<?php

/**
 * Transcoder for wowza
 *
 * To manage the Transcoder
 *
 * @name Transcoder
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Livestream\Lib\Wowza;

use Com\Wowza\Wowza; 

class Stream extends Wowza
{
    use Traits\WowzaDefault;

    public function __construct()
    {
        $this->initWowzaSettings();
        parent::__construct($this->wowza);
    }

    /**
     * function reset RestURI Publisher
     * 
     */
    private function resetRestURIStream(){
        $this->resetRestURI();
        $instanceName = '_definst_';
        $this->restURI = $this->restURI. '/vhosts/'.$this->getVHostInstance() . "/applications/" . env('WOWZA_APPLICATION') . "/instances/".$instanceName."/incomingstreams/";
    }

    public function getStatus($streamName){
        $this->resetRestURIStream();
        $this->restURI = $this->restURI . $streamName;
        return $this->sendRequest($this->preparePropertiesForRequest(self::class), [], self::VERB_GET);
    }
}
