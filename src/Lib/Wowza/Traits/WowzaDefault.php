<?php

/**
 * wowza trait with default functions
 *
 * To manage the Publishers
 *
 * @name Publisher
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Livestream\Lib\Wowza\Traits;

use Com\Wowza\Entities\Application\Helpers\Settings;

trait WowzaDefault {

    /**
     * @var object wowza
     * It holds the settings for wowza
     */
    private $wowza;

    /**
     * @var object version
     * It holds the version for api
     */
    private $version;

    /**
     * function to init Wowza Settings
     * 
     * @param string $version 
     */
    private function initWowzaSettings($version = 'v2'){
        $this->setVersion($version);
        $this->wowza = new Settings();
        $this->wowza->setHost(env('WOWZA_HOST').':8087/'.$this->version);
        $this->wowza->setUsername(env('WOWZA_USERNAME'));
        $this->wowza->setPassword(env('WOWZA_PASSWORD'));
    }

    /**
     * Funtion to reset RestURI
     * 
     */
    private function resetRestURI(){
        $this->restURI = $this->getHost() . "/servers/" . $this->getServerInstance();
    }

    /**
     * function to set Version for current module
     * 
     * @param string $version
     */
    private function setVersion($version){
        $this->version = $version;
    }
    
}