<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines used to display in admin user block.
    |
    */
    'livestream' => 'Livestream',
    'add_new_livestream' => 'Add new Livestream',
    'edit_livestream' => 'Edit Livestream',
    'max_height' => 'Maximum resolution',
    'resolutionhint' => 'Resolution only can be edited before the stream starts, Once the stream is initiated resolution cannot be changed.',
    'title' => 'Title',
    'description' => 'Description',
    'thumbnail' => 'Thumbnail',
    'streamdetails' => 'Stream details',
    'streamstatus' => 'Stream status',
    'scheduled_time' => 'Schedule Time',
    'imageuploadrecommendations' => 'Image format should be .jpeg,.jpg,.png, size should be below 2MB, minimum width of image should be 500px, dimension should be 4:3 ratio (i.e) 400*300',
];
