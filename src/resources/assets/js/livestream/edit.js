'use strict';
var add = angular.module('add', ['flow','moment-picker']).config(['momentPickerProvider', function (momentPickerProvider) {
    momentPickerProvider.options({
        locale:        'en',
        format:        'LLL',
        minView:       'decade',
        maxView:       'minute',
        startView:     'year',
        autoclose:     true,
        today:         false,
        keyboard:      true,
        
        /* Extra: Views properties */
        leftArrow:     '&larr;',
        rightArrow:    '&rarr;',
        yearsFormat:   'YYYY',
        monthsFormat:  'MMM',
        daysFormat:    'D',
        hoursFormat:   'HH:[00]',
        minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
        secondsFormat: 'ss',
        minutesStep:   5,
        secondsStep:   1,
    });
}]);

add.directive('baseValidator', validatorDirective);

add.factory('requestFactory', requestFactory);

add.controller('LiveController', function ($http, $scope, requestFactory, $rootScope, flowFactory) {
    this.info = {};
    this.responseMessage = false;
    this.showResponseMessage = false;
    $scope.errors = {};
    $scope.minDate = moment().add(30, 'minutes').format('LLL');
    $rootScope.requestParams = {
        grid: 'livestream'
    }
    $scope.rules = {
        title: 'required',
        description: 'required',
        is_active: 'required',
        scheduled_time: 'required',
        max_height:'required',
    }
    $scope.fetchedInfo = {};
    baseValidator.setRules($scope.rules);
    $scope.data = {
        title: '',
        description: '',
        is_active: '0',
        thumburl: '',
        scheduled_time: '',
        max_height:'',
    }
    $scope.init = function (id) {
        requestFactory.get(requestFactory.getUrl('livestream/' + id+'?presets=1'), function (response) {
            requestFactory.toggleLoader();
            $scope.fetchedInfo = response.response;
            $scope.presets =  response.presets;
            $scope.data = {
                id: response.response.id,
                title: response.response.title,
                description: response.response.description,
                is_active: '' + response.response.is_active,
                thumburl: response.response.thumburl,
                max_height: '' + response.response.max_height,
                scheduled_time: moment(new Date(response.response.scheduled_time +' GMT+0000')),
            }
        }, function (response) {
            if (response.status === 404) {
                alert('Live stream information not found');
                window.location = requestFactory.getTemplateUrl('admin/livestream');
            }
        });
    };
    $scope.existingFlowObject = flowFactory.create({
        target: document.querySelector('meta[name="base-api-url"]').getAttribute('content') + '/livestream/upload',
        permanentErrors: [404, 500, 501],
        headers: {
            'Accept': 'application/json'
        },
        testChunks: false,
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 1,
        singleFile: true
    });
    $scope.existingFlowObject.on('fileSuccess', function (event, message) {
        if (message) {
            delete($scope.errors.thumburl);
            $scope.data.thumburl = JSON.parse(message)['file-path'];
            $scope.data.properties = JSON.parse(message)['properties'];
            angular.element('.loaders').hide();
            angular.element('.submitbutton').attr('disabled', false);
        }
    });
    $scope.existingFlowObject.on('fileError', function (event, message, flowObject) {
        $scope.errors.thumburl = {
            has: 'true',
            message: JSON.parse(message).message.file[0]
        }
        setTimeout(function () {
            angular.element('#remove-file').trigger('click');
        }, 300);
        angular.element('.loaders').hide();
        angular.element('.submitbutton').attr('disabled', false);
    });
    $scope.existingFlowObject.on('fileAdded', function (file) {
        $scope.data.thumburl = '';
        if (!$scope.validateImage(file)) {
            return false;
        }
        angular.element('.loaders').show();
        angular.element('.submitbutton').attr('disabled', true)
    });
    $scope.triggerCancelfile = function () {
        $scope.data.thumburl = '';
    }
    $scope.resetFileUrl = function () {
        delete($scope.errors.thumburl);
        setTimeout(function () {
            angular.element('#remove-file').trigger('click');
        }, 300);
    }
    $scope.validateImage = function (file) {
        var oneMb = 1048576;
        delete($scope.errors.thumburl);
        if (file.size > oneMb * 2) {
            $scope.errors.thumburl = {
                has: true,
                message: 'Image file size should be below 2MB'
            }
            return false;
        } else if (['png', 'jpeg', 'jpg'].indexOf(file.getType()) === -1) {
            $scope.errors.thumburl = {
                has: true,
                message: 'Image format should be jpeg,jpg,png'
            }
            return false;
        }
        return true;
    }
    requestFactory.setThisArgument(this);
    angular.element('.alert-success').fadeIn(1000).delay(5000).fadeOut(1000);
    $scope.save = function ($event) {
        $scope.errors = {};
        if (baseValidator.validateAngularForm($event.target, $scope)) {
            requestFactory.toggleLoader();
            requestFactory.post(requestFactory.getUrl('livestream/add'), $scope.data, function (response) {
                window.location = requestFactory.getTemplateUrl('admin/livestream');
            }, $scope.fillError);
        }
    }
    /**
     * Method to handle the error message.
     */
    $scope.fillError = function (response) {
        requestFactory.toggleLoader();
        if (response.status == 422 && response.data.hasOwnProperty('message')) {
            angular.forEach(response.data.message, function (message, key) {
                if (typeof message == 'object' && message.length > 0) {
                    $scope.errors[key] = {
                        has: true,
                        message: message[0]
                    };
                }
            });
        } else {
            debugger;
        }
        console.log($scope.errors);
    };
});