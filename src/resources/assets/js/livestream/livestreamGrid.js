'use strict';

var LivestreamGridController = ['$scope', 'requestFactory', '$rootScope','$timeout', function (scope, requestFactory, $rootScope, $timeout) {
    var refreshtimeouttime = 30000;
    this.info = {};
    this.responseMessage = false;
    this.showResponseMessage = false;
    scope.errors = {};
    requestFactory.setThisArgument(this);
    scope.autoReload = false;
    angular.element('.alert-success').fadeIn(1000).delay(5000).fadeOut(1000);
    $rootScope.requestParams = {
        grid: 'livestream'
    }
    requestFactory.toggleLoader();

    scope.startStream = function (record) {
        requestFactory.toggleLoader();
        if (record.streamstatus === 'scheduled') {
            requestFactory.get(requestFactory.getUrl('livestream/startStream/' + record.id), function (response) {
                scope.getRecords(true);
                requestFactory.toggleLoader();
            }, function (response) {});
        }
    }
    scope.stopStream = function (record) {
        requestFactory.toggleLoader();
        if (record.streamstatus === 'live' || record.streamstatus === 'waiting') {
            requestFactory.get(requestFactory.getUrl('livestream/stopStream/' + record.id), function (response) {
                scope.getRecords(true);
                requestFactory.toggleLoader();
            }, function (response) {});
        }
    }
    /**
     *  Listen to the records to update property
     *  
     */
    scope.$on('afterGetRecords', function (e, data) {
        var count = 0;
        for(var i=0;i<data.data.data.length;i++) {
            if (['starting', 'waiting', 'live'].indexOf(data.data.data[i].streamstatus) > -1) {
                count++;
            }
        }
        scope.autoReload = (count)?true:false;
    });
    scope.validateAndFetchRecords = function(){
        if(scope.autoReload){
            scope.getRecords(true);
        }
        $timeout(scope.validateAndFetchRecords,refreshtimeouttime);
    }
    $timeout(scope.validateAndFetchRecords,refreshtimeouttime);
}];

window.gridControllers = {
    LivestreamGridController: LivestreamGridController
};
window.gridDirectives = {

};