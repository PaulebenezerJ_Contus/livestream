<div class="menu_container clearfix">
    <div class="page_menu pull-left">
        <ul class="nav">
            <li>
                <a href="{{url('admin/livestream')}}" data-ng-click="selectTab('normal_videos')" data-ng-class="{'active': requestParams.grid == 'livestream'}" >{{__('base::general.livevideos')}}</a>
            </li>
        </ul>
    </div>
</div>