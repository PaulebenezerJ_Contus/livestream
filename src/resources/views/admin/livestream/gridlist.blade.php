<div class="panel main_container">
    <div class="tab-content">
        <div class="tab-pane active" id="latest_video">
            <div class="tab_search clearfix">
                <div id="st-trigger-effects" class="search_upload_btn pull-right column">
                    <a href="{{url('admin/livestream/add')}}" data-effect="st-effect-18" class="btn btn-primary upload_video pull-right" data-intialize-sidebar>
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        {{trans('ads::ads.add_new')}}
                    </a>
                </div>
            </div>
            <div id="table_loader" class="table_loader_container" data-ng-show="gridLoadingBar">
                <div class="table_loader">
                    <div class="loader"></div>
                </div>
            </div>
            <div class="table_responsive">
                <table class="table playlist_table">
                    <thead>
                        <tr>
                            <th class="center">{{trans('base::general.s_no')}}</th>
                            <th data-ng-repeat="field in heading">
                                @{{field.name}}
                                <span data-ng-if="field.sort==true" id="" class="th-inner sortable both" data-ng-class="{showGridArrow:field.sort}" data-ng-click="fieldOrder($event,field.value)"></span>
                                <span data-ng-if="field.sort==false" data-ng-class="{showGridArrow:field.sort}"></span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="search_text">
                            <td></td>
                            <td class="search_product">
                                <input type="text" class="form-control" data-ng-model="searchRecords.title" data-boot-tooltip="true" title="{{trans('livestream::livestream.title')}}">
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                <select class="form-control mb15" data-ng-change="search()" data-ng-model="searchRecords.is_active" data-boot-tooltip="true"
                                    title="{{trans('base::general.select_status')}}">
                                    <option value="all">{{trans('base::general.all')}}</option>
                                    <option value='1'>{{trans('video::playlist.banner.active')}}</option>
                                    <option value='0'>{{trans('video::playlist.banner.inactive')}}</option>
                                </select>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td data-ng-if="noRecords" colspan="@{{heading.length + 1}}" class="no-data">{{trans('base::general.not_found')}}</td>
                        </tr>

                        <tr data-ng-if="showRecords" data-ng-repeat="record in records track by $index" data-ng-show="showRecords" class="list-repeat">
                            <td class="center">@{{((currentPage - 1) * rowsPerPage) + $index +1}}</td>
                            <td>
                                <div class="product_img">
                                    <a href="{{url('admin/livestream/view')}}/@{{record.id}}">
                                        <span class="img_title" >
                                            <img src="{{url('contus/base/images/no-preview.png')}}" data-ng-src="@{{ record.thumbnail }}" alt="" />
                                        </span>
                                        <label class="img_description">@{{record.title}}</label>
                                    </a>
                                </div>
                                <label class="live-schedule-wifi"><i class="fa fa-wifi" aria-hidden="true"></i>Scheduled  On : @{{ record.scheduled_time }}</label>
                            <td>
                                <span>
                                    <code>Wowza Push URL : @{{record.wowzapushurl}}</code></br>
                                    <code>Source Name : @{{record.streamname}}</code></br>
                                    <code>Username : admin</code></br>
                                    <code>Password : admin</code>
                                </span>
                            </td>
                            <td ng-switch="record.streamstatus">
                                <span class="label label-warning" ng-switch-when="starting" style="text-transform: capitalize;">@{{record.streamstatus}} </span>
                                <span class="label label-info" ng-switch-when="live" style="text-transform: capitalize;">@{{record.streamstatus}} </span>
                                <span class="label label-warning" ng-switch-when="waiting" style="text-transform: capitalize;">@{{record.streamstatus}} </span>
                                <span class="label label-success" ng-switch-when="completed" style="text-transform: capitalize;">@{{record.streamstatus}} </span>
                                <span class="label label-danger" ng-switch-when="unreachable" style="text-transform: capitalize;">@{{record.streamstatus}} </span>
                                <span class="label label-primary" ng-switch-default style="text-transform: capitalize;">@{{record.streamstatus}} </span>
                            </td>
                            <td>
                                <span class="label label-success" ng-if="record.is_active == 1" style="cursor: pointer;" data-ng-click="updateStatus(record)"
                                    title="{{trans('base::general.inactive')}} {{trans('livestream::livestream.livestream')}}" data-boot-tooltip="true">{{trans('video::playlist.message.active')}}</span>
                                <span class="label label-danger" ng-if="record.is_active != 1" style="cursor: pointer;" data-ng-click="updateStatus(record)"
                                    title="{{trans('base::general.active')}} {{trans('livestream::livestream.livestream')}}" data-boot-tooltip="true">{{trans('video::playlist.message.inactive')}}</span>
                            </td>
                            <td>@{{ $root.getFormattedDate(record.created_at) }}</td>
                            <td class="action left">
                                <a href="javascript:void(0)" ng-if="record.streamstatus == 'starting'" data-boot-tooltip="true" data-effect="st-effect-18" data-intialize-sidebar
                                    title="Starting.." class="table_action">
                                    <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                                </a>
                                <a href="javascript:void(0)" ng-if="record.streamstatus == 'live' || record.streamstatus == 'waiting'" data-boot-tooltip="true" data-effect="st-effect-18" data-intialize-sidebar
                                    title="Stop" data-ng-click="stopStream(record)" class="table_action">
                                    <i class="fa fa-stop-circle" aria-hidden="true"></i>
                                </a>
                                <a href="javascript:void(0)" ng-if="record.streamstatus == 'scheduled'" data-boot-tooltip="true" data-effect="st-effect-18" data-intialize-sidebar
                                    title="Start" data-ng-click="startStream(record)" class="table_action">
                                    <i class="fa fa-play-circle" aria-hidden="true"></i>
                                </a>
                                <a ng-if="['completed','unreachable'].indexOf(record.streamstatus) == '-1'" href="{{url('admin/livestream/edit')}}/@{{record.id}}" data-boot-tooltip="true" data-effect="st-effect-18" data-intialize-sidebar
                                    title="Edit" class="table_action">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <span ng-if="['completed','waiting'].indexOf(record.streamstatus) == '-1'" ng-mouseover="getTooltip($event)" title="{{trans('base::general.delete')}}" data-toggle="modal" data-target="#deleteModal"
                                    ng-click="deleteSingleRecord(record.id)" class="tooltips delete_table_icon" data-boot-tooltip="true"
                                    data-original-title="">
                                    <i class="fa fa-trash-o"></i>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @include('base::layouts.pagination')
        </div>
    </div>
</div>