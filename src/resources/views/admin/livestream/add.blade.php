@extends('base::layouts.default') @section('stylesheet')
<link rel="stylesheet" href="{{$getBaseAssetsUrl('css/bootstrap-fileupload.min.css')}}" />
<link rel="stylesheet" href="{{$getBaseAssetsUrl('css/angular-moment-picker.min.css')}}" />
<link rel="stylesheet" href="{{$getBaseAssetsUrl('css/uploader.css')}}" /> @endsection @section('header') @include('base::layouts.headers.dashboard') @endsection @section('content')
<div data-ng-app="add" data-ng-controller="LiveController" data-ng-init="init()">
    @include('livestream::admin.common.subMenu')
    <form name="userForm" method="POST" data-base-validator data-ng-submit="save($event)" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="contentpanel">
            @include('base::partials.errors')
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="add_form detail_video_form clearfix">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <h2>
                                                <span>{{trans('livestream::livestream.add_new_livestream')}}</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" data-ng-class="{'has-error': errors.title.has}">
                                            <label class="control-label">{{trans('livestream::livestream.title')}}
                                                <span class="asterisk">*</span>
                                            </label>
                                            <input type="text" ng-model="data.title" data-validation-name="{{trans('livestream::livestream.title')}}" name="title" class="form-control"
                                                placeholder="{{trans('livestream::livestream.title')}}" value="{{old('title')}}" />
                                            <p class="help-block" data-ng-show="errors.title.has">@{{errors.title.message }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" data-ng-class="{'has-error': errors.description.has}">
                                            <label class="control-label">{{trans('livestream::livestream.description')}}
                                                <span class="asterisk">*</span>
                                            </label>
                                            <textarea type="text" ng-model="data.description" data-validation-name="{{trans('livestream::livestream.description')}}" name="description" class="form-control"
                                                placeholder="{{trans('livestream::livestream.description')}}" value="{{old('description')}}" ></textarea>
                                            <p class="help-block" data-ng-show="errors.description.has">@{{errors.description.message }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" data-ng-class="{'has-error': errors.scheduled_time.has}">
                                            <label class="control-label">{{trans('livestream::livestream.scheduled_time')}}
                                                <span class="asterisk">*</span>
                                            </label>
                                            <input class="form-control" readonly="true"
                                                style="background-color: #fff;"
                                                ng-model="data.scheduled_time"
                                                name="scheduled_time"
                                                ng-model-options="{ updateOn: 'blur' }"
                                                placeholder="{{trans('livestream::livestream.scheduled_time')}}"
                                                moment-picker="data.scheduled_time"
                                                data-min-date="minDate"/>
                                            <p class="help-block" data-ng-show="errors.scheduled_time.has">@{{errors.scheduled_time.message }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" data-ng-class="{'has-error': errors.status.has}">
                                            <label class="control-label">{{trans('user::adminuser.status')}}</label>
                                            <select class="form-control mb10" name="is_active" ng-model="data.is_active">
                                                <option value="1">{{trans('user::adminuser.active')}}</option>
                                                <option value="0">{{trans('user::adminuser.inactive')}}</option>
                                            </select>
                                            <p class="help-block" data-ng-show="errors.status.has">@{{errors.status.message }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" data-ng-class="{'has-error': errors.max_height.has}">
                                            <label class="control-label">{{trans('livestream::livestream.max_height')}}</label>
                                            <select class="form-control mb10" data-validation-name="{{trans('livestream::livestream.max_height')}}" name="max_height" ng-model="data.max_height">
                                                <option ng-repeat="preset in presets" value="@{{preset.height}}" >@{{preset.title}} (@{{preset.width}}x@{{preset.height}})</option>
                                            </select>
                                            <p class="intimation" data-ng-if="!errors.max_height.has ">{{__('livestream::livestream.resolutionhint')}}</p>
                                            <p class="help-block" data-ng-show="errors.max_height.has">@{{errors.max_height.message }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" data-ng-class="{'has-error': errors.thumburl.has}">
                                            <label class="control-label">{{trans('livestream::livestream.thumbnail')}}
                                            </label>
                                            <div flow-object="existingFlowObject" flow-init flow-file-added="!!{png:1,jpg:1,jpeg:1,mp4:1}[$file.getExtension()]" flow-files-submitted="$flow.upload()">
                                                <div class="">
                                                    <hr class="soften" />
                                                    <div>
                                                        <div class="thumbnail" ng-hide="$flow.files.length">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" />
                                                        </div>
                                                        <div class="thumbnail" ng-show="$flow.files.length">
                                                            <img flow-img="$flow.files[0]" />
                                                        </div>
                                                        <div>
                                                            <a href="#" class="btn btn-primary upload_video" ng-hide="$flow.files.length" flow-btn
                                                                flow-attrs="{accept:'image/*'}">
                                                                <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                                                {{__('base::general.select')}} {{__('base::general.image')}}
                                                            </a>
                                                            <a href="#" class="btn btn-default " ng-show="$flow.files.length" flow-btn flow-attrs="{accept:'image/*'}">{{__('base::general.change')}}</a>
                                                            <a href="#" class="btn btn-danger" id="remove-file" ng-show="$flow.files.length" ng-click="$flow.cancel();triggerCancelfile()"> {{__('base::general.remove')}} </a>
                                                            <span class="loaders" id="loader" style="display: none">
                                                                <img src="{{ url('contus/base/images/admin/loader.gif') }}" alt="ImageLoader" height="100" width="100">
                                                            </span>
                                                        </div>
                                                        <p class="intimation" data-ng-if="!errors.thumburl.has ">{{__('livestream::livestream.imageuploadrecommendations')}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <input ng-model="data.thumburl" name="thumburl" type="text" data-validation-name="File" data-ng-hide="true" />
                                            <p class="help-block" data-ng-show="errors.thumburl.has">@{{errors.thumburl.message }}</p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="padding10">
            <div class="fixed-btm-action">
                <div class="text-right btn-invoice">
                    <a class="btn btn-white mr5" href="{{url('admin/livestream')}}">{{trans('base::general.cancel')}}</a>
                    <button class="btn btn-primary submitbutton">{{trans('base::general.submit')}}</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection @section('scripts')
<script src="{{$getBaseAssetsUrl('js/ng-flow-standalone.js')}}"></script>
<script src="{{$getBaseAssetsUrl('js/Validate.js')}}"></script>
<script src="{{$getBaseAssetsUrl('js/moment-with-locales.js')}}"></script>
<script src="{{$getBaseAssetsUrl('js/angular-moment-picker.min.js')}}"></script>
<script src="{{$getBaseAssetsUrl('js/validatorDirective.js')}}"></script>
<script src="{{$getBaseAssetsUrl('js/requestFactory.js')}}"></script>
<script src="{{$getLivestreamAssetsUrl('js/livestream/add.js')}}"></script>

@endsection