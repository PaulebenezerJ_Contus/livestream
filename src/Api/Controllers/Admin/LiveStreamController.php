<?php

/**
 * Livestream Controller
 *
 * To manage the Livestream such as update status
 *
 * @name Livestream Controller
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Livestream\Api\Controllers\Admin;

use Contus\Base\ApiController;
use Contus\Livestream\Repositories\LivestreamRepository;

class LivestreamController extends ApiController
{
    /**
     * Constructer method which defines the objects of the classes used.
     *
     */
    public function __construct()
    {

        parent::__construct();
        $this->repository = new LivestreamRepository();
        $this->repository->setRequestType(static::REQUEST_TYPE);
    }

    /**
     * function to get postUploads
     * @return json
     */
    public function postUploads()
    {
        try {
            $this->repository->validateImage();
            $file = $this->request->file('file');
            $fileName = time() . $file->hashName();
            $filePath = 'live/img/' . $fileName;
            $fileInfo = [
                'file-path' => $filePath,
            ];
            \Image::make($file)->resize(400, 300)->save($file->path());
            $s3 = \Storage::disk('s3');
            $s3->put($filePath, file_get_contents($file), 'public');
            return response()->json($fileInfo, 200);
        } catch (ValidationException $e) {
            return ($e->response) ? ($e->response) : response()->json($e->validator->errors()->getMessages(), 422);
        }
    }

    /**
     * function to add ads
     *
     * @return json
     */
    public function postAdd()
    {
        try {
            $this->repository->addOrUpdate();
            return $this->getSuccessJsonResponse(['message' => 'success']);
        } catch (ValidationException $e) {
            return $this->getErrorJsonResponse([], $e->validator->errors()->getMessages(), 422);
        } catch (\Exception $exception) {
            return $this->getErrorJsonResponse([], (method_exists($exception, 'getMessage') && env('APP_DEBUG', false)) ? $exception->getMessage() : trans('base::general.invalid_request'), (method_exists($exception, 'getStatusCode')) ? $exception->getStatusCode() : 500);
        }
    }

    /**
     * function to get Livestream
     * @param integer $id 
     * @return json 
     */
    public function getLivestream($id)
    {
        try {
            $live = $this->repository->getLivestream($id);
            if ($this->request->input('presets')) {
                $presets = $this->repository->getLivepresets();
            }
            return $this->getSuccessJsonResponse(['message' => 'success', 'presets' => $presets, 'response' => $live]);
        } catch (\Exception $exception) {
            return $this->getErrorJsonResponse([], (method_exists($exception, 'getMessage') && env('APP_DEBUG', false)) ? $exception->getMessage() : trans('base::general.invalid_request'), (method_exists($exception, 'getStatusCode')) ? $exception->getStatusCode() : 500);
        }
    }

    /**
     * function to get Info
     * @return json 
     */
    public function getInfo()
    {
        try {
            return $this->getSuccessJsonResponse(['message' => 'success', 'presets' => $this->repository->getLivepresets()]);
        } catch (\Exception $exception) {
            return $this->getErrorJsonResponse([], (method_exists($exception, 'getMessage') && env('APP_DEBUG', false)) ? $exception->getMessage() : trans('base::general.invalid_request'), (method_exists($exception, 'getStatusCode')) ? $exception->getStatusCode() : 500);
        }
    }

    /**
     * Common post Action request controller
     *
     * @return object records
     */
    public function postAction() {
        $this->repository->deleteLiveFiles();
        return $this->repository->prepareGrid ()->action () ? $this->getSuccessJsonResponse ( [ ], trans ( 'base::general.success_delete' ) ) : $this->getErrorJsonResponse ( [ ], trans ( 'base::general.invalid_request' ), 403 );
    }

    public function startStream($id){
        try {
            return $this->getSuccessJsonResponse(['message' => $this->repository->processStream($id)]);
        } catch (\Exception $exception) {
            return $this->getErrorJsonResponse([], (method_exists($exception, 'getMessage') && env('APP_DEBUG', false)) ? $exception->getMessage() : trans('base::general.invalid_request'), (method_exists($exception, 'getStatusCode')) ? $exception->getStatusCode() : 500);
        }
    }

    public function stopStream($id){
        try {
            return $this->getSuccessJsonResponse(['message' => $this->repository->endStream($id)]);
        } catch (\Exception $exception) {
            return $this->getErrorJsonResponse([], (method_exists($exception, 'getMessage') && env('APP_DEBUG', false)) ? $exception->getMessage() : trans('base::general.invalid_request'), (method_exists($exception, 'getStatusCode')) ? $exception->getStatusCode() : 500);
        }
    }

}
