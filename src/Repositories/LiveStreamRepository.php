<?php

/**
 * Livestream Repository
 *
 * To manage the functionalities related to the Livestream module from Livestream Controller
 *
 * @name LivestreamRepository
 * @author Contus<developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */

namespace Contus\Livestream\Repositories;

use Contus\Base\Repository as BaseRepository;
use Contus\Livestream\Lib\Wowza\SmilFile;
use Contus\Livestream\Lib\Wowza\Transcoder;
use Contus\Livestream\Lib\Wowza\Stream;
use Contus\Livestream\Models\LivePresets;
use Contus\Livestream\Models\Livestream;
use Illuminate\Support\Carbon;
use stdClass;

class LivestreamRepository extends BaseRepository
{
    /**
     * Get headings for grid
     *
     * @return array
     */
    public function prepareGrid()
    {
        return $this->setGridModel(new Livestream());
    }

    /**
     * Function to apply filter for search of Comments grid
     *
     * @param mixed $search
     * @return \Illuminate\Database\Eloquent\Builder $search The builder object of comments grid.
     */
    protected function searchFilter($search)
    {
        $searchRecordGroup = $this->request->has('searchRecord') && is_array($this->request->input('searchRecord')) ? $this->request->input('searchRecord') : [];
        $title = $is_active = null;
        extract($searchRecordGroup);
        if ($title) {
            $search = $search->where('title', 'like', '%' . $title . '%');
        }
        if (is_numeric($is_active)) {
            $search = $search->where('is_active', $is_active);
        }
        return $search->selectRaw('livestream.*, livestream.id as thumbnail');
    }

    /**
     * function to get Livestream
     * @param integer $id 
     * @return mixed 
     */
    public function getLivestream($id)
    {
        $data = (new Livestream())->selectRaw('livestream.*, livestream.id as thumbnail')->find($id);
        if ($data) {
            return $data;
        } else {
            abort('404', __('base::general.resource_not_exist'));
        }
    }

    /**
     * function to validate uploadin file
     */
    public function validateImage()
    {
        $this->setRule('file', 'required|mimes:jpeg,jpg,png|max:2048|dimensions:min_width=400,ratio=400/300');
        $this->_validate();
    }

    /**
     * function to add Or Update
     * @return mixed
     */
    public function addOrUpdate()
    {
        $this->setRules([
            'title' => 'required',
            'description' => 'required',
            'is_active' => 'required',
        ]);
        $this->_validate();
        $id = $this->request->has('id') ? $this->request->get('id') : '';
        $liveStream = (new Livestream())->findOrNew($id);
        $liveStream->fill($this->request->all());
        $liveStream->scheduled_time = new Carbon($this->request->scheduled_time);
        if ($liveStream->save()) {
            if (!$this->request->has('id')) {
                $stringRandom = $this->generateRand(5) . time();
                $liveStream->streamname = $stringRandom;
                //scheduled, processing, ready, started, terminated.
                $liveStream->streamstatus = 'scheduled';
                $liveStream->wowzapushurl = env('WOWZA_HOST') . ':1935/live';
                $liveStream->save();
            }
            return true;
        }
        abort(500, 'ERROR');
    }

    /**
     * function to get Livepresets
     * 
     * @return mixed 
     */
    public function getLivepresets()
    {
        return LivePresets::where('is_active', 1)->get();
    }

    public function deleteLiveFiles(){
        $deleteIds = $this->request->selectedCheckbox;
        foreach($deleteIds as $id){
            $dlive = (new Livestream())->find($id);
            (new SmilFile())->deleteSmilFileAction($dlive->streamname);
            if($dlive->thumburl){
                $s3 = \Storage::disk('s3');
                $s3->delete($dlive->thumburl);
            }
        }
    }

    /**
     * function to generate StreamSmil
     * 
     * @param mixed $encodes 
     * @param mixed $liveStream 
     * @return mixed 
     */
    private function generateStreamSmil($encodes, $liveStream)
    {
        $streams = [];
        foreach ($encodes as $encode) {
            $stream = new stdClass();
            $stream->systemLanguage = $encode->systemLanguage;
            $stream->systemBitrate = "";
            $stream->src =  $liveStream->streamname.'_'.(($liveStream->max_height == $encode->height)?'source':$encode->title);
            $stream->type = $encode->sourcetype;
            $stream->audioBitrate = $encode->audioBitrate;
            $stream->videoBitrate = $encode->videoBitrate;
            $stream->width = $encode->width;
            $stream->height = $encode->height;
            $streams[] = $stream;
        }
        return $streams;
    }

    /**
     * function to process Stream
     * 
     * @param integer $id 
     * @return mixed 
     */
    public function processStream($id)
    {
        $liveStream = (new Livestream())->find($id);
        $liveStream->streamstatus = 'starting';
        $scheduledTime = new Carbon($liveStream->scheduled_time);
        $now = new Carbon();
        if($scheduledTime->gte($now)){
            $liveStream->scheduled_time = Carbon::now()->toDateTimeString();
        }
        $liveStream->updated_at = new Carbon();
        $liveStream->save();
        $transcoderPresets = LivePresets::where('height', '<=', 1080)->where('is_active',1)->orderBy('height','DESC')->get();
        $streams = $this->generateStreamSmil($transcoderPresets, $liveStream);
        $response = (new SmilFile())->addSmilFile($liveStream->streamname, $streams);
        if (isset($response->code) && $response->code == '409') {
            $response = (new SmilFile())->updateSmilFile($liveStream->streamname, $streams);
        }
        (new SmilFile())->updateSmilFileAction($liveStream->streamname,'connect');
        $liveStream->liveurl = env('WOWZA_CDN').'/live/smil:'.$liveStream->streamname.'.smil/playlist.m3u8';
        $liveStream->save();
        return $response->success;
    }

    public function endStream($id){
        $completed = (new Livestream())->where('streamstatus','live')->where('id', $id)->count();
        if($completed>0){
            $record = (new Livestream())->where('streamstatus','live')->where('id', $id)->first();
            (new SmilFile())->deleteSmilFileAction($record->streamname);
            $record->streamstatus = 'completed';
            $record->updated_at = new Carbon();
            return $record->save();
        }
        return false;
    }

    /**
     * function to generate random text
     *
     * @param integer $length
     *
     * @return string
     */
    public function generateRand($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function initLiveVideos(){
        echo Carbon::now()->toDateTimeString();
        $records = (new Livestream())->where('streamstatus','scheduled')->where('scheduled_time', '<', Carbon::now()->toDateTimeString())->pluck('id');
        foreach($records as $id){
            $this->processStream($id);
        }
    }

    public function statusLiveStreamAll(){
        $this->trackWaitingStartingStatus();
        $this->updateStartingLiveStatus();
    }

    private function updateStartingLiveStatus(){
        $records = (new Livestream())->where('streamstatus','starting')->get();
        foreach($records as $record){
            $check = (new Stream())->getStatus( $record->streamname);
            if(isset($check->isConnected) && $check->isConnected){
                $record->streamstatus = 'live';
                $record->save();
            }
        }
        $records = (new Livestream())->where('streamstatus','waiting')->get();
        foreach($records as $record){
            $check = (new Stream())->getStatus( $record->streamname);
            if(isset($check->isConnected) && $check->isConnected){
                $record->streamstatus = 'live';
                $record->updated_at = new Carbon();
                $record->save();
            }
        }
        $records = (new Livestream())->where('streamstatus','live')->get();
        foreach($records as $record){
            $check = (new Stream())->getStatus( $record->streamname);
            if(isset($check->isConnected) && $check->isConnected){
                $record->streamstatus = 'live';
            }else{
                $record->streamstatus = 'waiting';
                $record->updated_at = new Carbon();
                $record->save();
            }
        }
    }

    private function trackWaitingStartingStatus(){
        $completed = (new Livestream())->where('streamstatus','waiting')->where('updated_at', '<', Carbon::now()->subMinutes(10)->toDateTimeString())->count();
        if($completed>0){
            $records = (new Livestream())->where('streamstatus','waiting')->where('updated_at', '<', Carbon::now()->subMinutes(10)->toDateTimeString())->get();
            foreach($records as $record){
                (new SmilFile())->deleteSmilFileAction($record->streamname);
                $record->streamstatus = 'completed';
                $record->updated_at = new Carbon();
                $record->save();
            }
        }
        $completed = (new Livestream())->where('streamstatus','starting')->where('updated_at', '<', Carbon::now()->subMinutes(10)->toDateTimeString())->count();
        if($completed>0){
            $records = (new Livestream())->where('streamstatus','starting')->where('updated_at', '<', Carbon::now()->subMinutes(10)->toDateTimeString())->get();
            foreach($records as $record){
                (new SmilFile())->deleteSmilFileAction($record->streamname);
                $record->streamstatus = 'unreachable';
                $record->updated_at = new Carbon();
                $record->save();
            }
        }
    }

    /**
     * Get headings for grid
     *
     * @return array
     */
    public function getGridHeadings()
    {
        return [
            'heading' => [
                [
                    'name' => __('livestream::livestream.title'),
                    'value' => 'title',
                    'sort' => true,
                ],
                [
                    'name' => trans('livestream::livestream.streamdetails'),
                    'value' => 'streamname',
                    'sort' => false,
                ],
                [
                    'name' => trans('livestream::livestream.streamstatus'),
                    'value' => 'streamstatus',
                    'sort' => false,
                ],
                [
                    'name' => trans('user::adminuser.status'),
                    'value' => 'status',
                    'sort' => false,
                ],
                [
                    'name' => trans('base::general.added_on'),
                    'value' => 'created_at',
                    'sort' => false,
                ],
                [
                    'name' => trans('base::general.action'),
                    'value' => 'Action',
                    'sort' => false,
                ],
            ],
        ];
    }
}
